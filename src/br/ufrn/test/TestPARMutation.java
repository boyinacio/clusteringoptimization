package br.ufrn.test;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.operators.crossover.HMCRCrossover;
import jmetal.operators.mutation.PARMutation;
import jmetal.problems.clustering.SimpleClusterNumberProblem;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.distances.JoaoCarlosDistance;

public class TestPARMutation {

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws JMException
	 */
	public static void main(String[] args) throws ClassNotFoundException,
			JMException {
		
		for (int i = 0; i < 10; i++) {
			int good = 0, bad = 0;
			for (int j = 0; j < 100; j++) {
				if(go()){
					good++;
				} else {
					bad++;
				}
			}
			
			System.out.println("Good: " + good + " | Bad: " + bad);	
		}
		
		
		
	}

	public static boolean go() throws ClassNotFoundException, JMException {
		String base = "glass";
		Instances baseDados = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		SimpleClusterNumberProblem problem = new SimpleClusterNumberProblem(
				baseDados, new JoaoCarlosDistance(), 0.8, 7);

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("PAR", 0.5);
		parameters.put("HMRC", 0.45);
		HMCRCrossover crossover = new HMCRCrossover(parameters);
		PARMutation mutation = new PARMutation(parameters);

		SolutionSet population = new SolutionSet(20);
		Solution S;

		for (int i = 0; i < 20; i++) {
			S = new Solution(problem);
			problem.evaluate(S);
			population.add(S);
		}

		SolutionSet newPopulation = (SolutionSet) crossover.execute(population);

		// print(population);
		// System.out.println();
		// System.out.println("Best: " + population.best(new
		// SingleObjectiveComparatorMaximum()));
		// System.out.println();
		// print(newPopulation);
		// System.out.println();
		// System.out.println("Best: " + newPopulation.best(new
		// SingleObjectiveComparatorMaximum()));
		// System.out.println();
		// printS(newPopulation);

		Solution T = newPopulation.best(new SingleObjectiveComparatorMaximum());
		Solution TM = (Solution) mutation.execute(T);
		problem.evaluate(TM);

//		print(T);
//		System.out.println("Fitness: " + T.getObjective(0));
//		print(TM);
//		System.out.println("Fitness: " + TM.getObjective(0));
		
		return (T.getObjective(0) > TM.getObjective(0));
	}

	public static void print(SolutionSet population) {
		Solution S;
		for (int i = 0; i < population.size(); i++) {
			S = population.get(i);
			System.out.println(S.getObjective(0));
		}
	}

	public static void printS(SolutionSet population) throws JMException {
		Solution S;
		for (int i = 0; i < population.size(); i++) {
			S = population.get(i);
			print(S);
			System.out.println();
		}
	}

	public static void print(Solution S) throws JMException {
		Variable[] V = S.getDecisionVariables();

		for (int i = 0; i < V.length; i++) {
			System.out.print(V[i].getValue() + " ");
		}

	}

}
