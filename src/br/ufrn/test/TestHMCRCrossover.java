package br.ufrn.test;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.operators.crossover.HMCRCrossover;
import jmetal.problems.clustering.SimpleClusterNumberProblem;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.distances.JoaoCarlosDistance;

public class TestHMCRCrossover {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws JMException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, JMException {
		
		String base = "glass";
		Instances baseDados = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
		
		SimpleClusterNumberProblem problem = new SimpleClusterNumberProblem(baseDados,
				 new JoaoCarlosDistance(), 0.8, 7);
		
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("HMCR", 0.45);
		HMCRCrossover crossover = new HMCRCrossover(parameters);
		
		SolutionSet population = new SolutionSet(20);
		Solution S;
		
		for (int i = 0; i < 20; i++) {
			S = new Solution(problem);
			problem.evaluate(S);
			population.add(S);
		}
		
		SolutionSet newPopulation = (SolutionSet) crossover.execute(population);
		print(population);
		System.out.println();
		System.out.println("Best: " + population.best(new SingleObjectiveComparatorMaximum()));
		System.out.println();
		print(newPopulation);
		System.out.println();
		System.out.println("Best: " + newPopulation.best(new SingleObjectiveComparatorMaximum()));
		System.out.println();
		printS(newPopulation);
		
		
	}
	
	public static void print(SolutionSet population){
		Solution S;
		for (int i = 0; i < population.size(); i++) {
			S = population.get(i);
			System.out.println(S.getObjective(0));
		}
	}
	
	public static void printS(SolutionSet population) throws JMException{
		Solution S;
		for (int i = 0; i < population.size(); i++) {
			S = population.get(i);
			print(S);
			System.out.println();
		}
	}
	
	public static void print(Solution S) throws JMException{
		Variable[] V = S.getDecisionVariables();
		
		for (int i = 0; i < V.length; i++) {
			System.out.print(V[i].getValue() + " ");
		}
		
	}

}
