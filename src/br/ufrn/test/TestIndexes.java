package br.ufrn.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.swing.JFileChooser;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.BasicDaviesBouldin;
import zelig.util.clusteringEvaluators.FiltraExtensoes;
import zelig.util.clusteringEvaluators.Sillhouette;

public class TestIndexes {

	private static File[] files;
	private static Instances original;
	
	/**a
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe a base: ");
		loadOriginalBase(sc.nextLine());
		AbrirTodos();
		System.out.println("Base\tDBIndex\tSillhouette\tARI");		
		
		for (int i = 0; i < files.length; i++) {
			FileInputStream inFile = new FileInputStream(files[i]);
			InputStreamReader in = new InputStreamReader(inFile);

			Instances base = new Instances(in);
			base.setClassIndex(base.numAttributes() - 1);
			
			System.out.println(base.relationName() + "\t"
					+ BasicDaviesBouldin.main(base) + "\t"
					+ AdjustedRandIndex.ARI(original, base));

		}

	}

	private static void AbrirTodos() {
		String url = ".";

		JFileChooser chooser = null;
		chooser = new JFileChooser(url);
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setMultiSelectionEnabled(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			files = chooser.getSelectedFiles();
		}
	}
	
	private static void loadOriginalBase(String baseName){
		try {
			DataSource source = new DataSource("bases/" + baseName + ".arff");
			original = source.getDataSet();
			original.setClassIndex(original.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
