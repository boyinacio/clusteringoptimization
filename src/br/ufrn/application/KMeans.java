package br.ufrn.application;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import weka.clusterers.SimpleKMeans;
import weka.core.*;

public class KMeans {

   

    public static void main(String[] args) throws Exception {

        SimpleKMeans agrupamento = new SimpleKMeans();
        
       

        FileReader reader = new FileReader("bases/all/iris.arff");
        Instances instancias = new Instances(reader);

        instancias.deleteAttributeAt(4);
        agrupamento.setNumClusters(3);
        agrupamento.setDisplayStdDevs(true);
        agrupamento.buildClusterer(instancias);
        

        Instances clusterCenters = agrupamento.getClusterCentroids();
        Instances clusterSTDDEVs = agrupamento.getClusterStandardDevs();
        int[] clusterSizes = agrupamento.getClusterSizes();
        for (int cluster = 0;cluster < clusterCenters.numInstances(); cluster++) {
            System.out.println(" Cluster #" + (cluster + 1) + ": " + clusterSizes[cluster] + " dados");
            System.out.println(" Centr�ide : " + clusterCenters.instance(cluster));
            System.out.println(" STDDEV : " + clusterSTDDEVs.instance(cluster));
        }
        // * System.out.println("n�mero de inst�ncias:" +
        //  * instancias.numInstances());
        //  *
        //  * for (int i = 0; i < 4; i++) { int nC =
        //  * agrupamento.clusterInstance(instancias.instance(i)); if (i % 4 == 0)
        //   * { System.out.println("\n\nclasse\n"); } double[] d =
        //  * agrupamento.distributionForInstance(instancias.instance(i)); for
        //   * (double di : d) { System.out.println(di); }
        //  * System.out.println("\n\n");
        //  *
        //  * }
        //   *
        
     
        List grupos = new ArrayList();
        
        //for (int cluster = 0;cluster < clusterCenters.numInstances(); cluster++) {
        for (int i = 0; i < instancias.numInstances(); i++) {
            try {
                int nC = agrupamento.clusterInstance(instancias.instance(i));
                System.out.println("Instancia " + i + ": Grupo " + nC);
                //grupos.add(nC);
                //System.out.println(grupos);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        }
        
                
    
}

