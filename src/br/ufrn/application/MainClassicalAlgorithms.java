package br.ufrn.application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jmetal.util.PseudoRandom;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.translators.ClusterNumberTranslator;
import br.ufrn.util.Data;

public class MainClassicalAlgorithms {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		LinkedList<String> bases = new LinkedList<String>();

		bases.add("autos");
		bases.add("breast-cancer");
		bases.add("proteina_mod");
		bases.add("segment_mod");

		for (String base : bases) {
			System.out.println("Base: " + base);
			run(base);
		}
	}

	public static void run(String base) {
		Instances baseDados = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		int size = baseDados.numClasses();
		int minGrupos, maxGrupos;

		if (size < 4) {
			minGrupos = 2;
			maxGrupos = 5;
		} else {
			minGrupos = size - 2;
			maxGrupos = size + 2;
		}

		SimpleKMeans kmeans;
		int seed;

		// DBScan dbscan;

		ArrayList<Data> list = new ArrayList<Data>();
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		double[] rand, db;
		Data best, worst;

		Instances copy = new Instances(baseDados);
		Remove filter = new Remove();

		try {
			filter.setAttributeIndices("" + (baseDados.classIndex() + 1));
			filter.setInputFormat(baseDados);
			copy = Filter.useFilter(baseDados, filter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		for (int g = minGrupos; g <= maxGrupos; g++) {
			seed = PseudoRandom.randInt(1000, 10000);

			kmeans = new SimpleKMeans();
			kmeans.setPreserveInstancesOrder(true);

			// dbscan = new DBScan();
			// dbscan.setEpsilon(0.8);

			for (int exp = 1; exp <= 1; exp++) {
				try {
					kmeans.setNumClusters(g);
					kmeans.setSeed(seed);
					kmeans.buildClusterer(copy);
				} catch (Exception e3) {
					e3.printStackTrace();
					System.exit(1);
				}

				// try {
				// dbscan.buildClusterer(copy);
				// } catch (Exception e2) {
				// e2.printStackTrace();
				// System.exit(1);
				// }

				int[] labels = new int[copy.numInstances()];
				int sz = 0;

				try {
					sz = kmeans.numberOfClusters();
				} catch (Exception e2) {
					e2.printStackTrace();
					System.exit(1);
				}
				
				List grupos = new ArrayList();
		        
		        //for (int cluster = 0;cluster < clusterCenters.numInstances(); cluster++) {
		        for (int i = 0; i < copy.numInstances(); i++) {
		            try {
		                int nC = kmeans.clusterInstance(copy.instance(i));
		                grupos.add(nC);
		                System.out.println(grupos);
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		        }

//				for (int i = 0; i < copy.numInstances(); i++) {
//					try {
//						labels[i] = kmeans.clusterInstance(copy.instance(i));
//					} catch (Exception e) {
//						labels[i] = sz + 1;
//					}
//
//				}
//
//				copy = translator.translate(labels, copy);
//
//				try {
//					list.add(new Data(copy.numClasses(), BasicDaviesBouldin
//							.main(copy), AdjustedRandIndex.ARI(baseDados, copy)));
//				} catch (IOException e1) {
//					e1.printStackTrace();
//					System.exit(1);
//				}
//
//				try {
//					copy = Filter.useFilter(baseDados, filter);
//				} catch (Exception e) {
//					e.printStackTrace();
//					System.exit(1);
//				}
//
//			}
//
//			Collections.sort(list);
//
//			db = new double[list.size()];
//			rand = new double[list.size()];
//			for (int i = 0; i < db.length; i++) {
//				db[i] = list.get(i).getDb();
//				rand[i] = list.get(i).getRand();
//			}
//
//			best = list.get(list.size() - 1);
//			worst = list.get(0);
//
//			System.out.printf("%d\t%.15f\t%.15f\n", best.getClusters(),
//					best.getDb(), best.getRand());
//			System.out.printf("%d\t%.15f\t%.15f\n", worst.getClusters(),
//					worst.getDb(), worst.getRand());
//			System.out.printf("-\t%.15f\t%.15f\n", Statistics.mean(db),
//					Statistics.mean(rand));
//			System.out.printf("-\t%.15f\t%.15f\n", Statistics.deviation(db),
//					Statistics.deviation(rand));
//
//			list.clear();
		}

	}

}
