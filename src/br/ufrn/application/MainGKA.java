package br.ufrn.application;

import java.util.Comparator;
import java.util.LinkedList;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.gka.GKA;
import jmetal.problems.clustering.GKA_Problem;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.Printer;
import zelig.util.distances.JoaoCarlosDistance;
import br.ufrn.util.GKASettingsLoader;

public class MainGKA {

	public static void main(String[] args) throws Exception {
		LinkedList<String> bases = new LinkedList<String>();

		bases.add("autos");
		bases.add("sonar");
		bases.add("glass");
		bases.add("breast-cancer");
		bases.add("proteina_mod");
		bases.add("solar-flare");
		bases.add("ecoli_mod");
		bases.add("segment_mod");
		bases.add("ionosphere");
		bases.add("dermatology");

		for (String base : bases) {
			run(base);
		}
	}

	public static void run(String base) throws Exception {
		Instances baseDados = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		int size = baseDados.numClasses();
		int minGrupos, maxGrupos;

		if (size < 4) {
			minGrupos = 2;
			maxGrupos = 5;
		} else {
			minGrupos = size - 2;
			maxGrupos = size + 2;
		}

		int populacao = 50;
		int geracoes = 100;
		double mut_prob = 0.05;

		GKASettingsLoader settingsLoader;

		// GKA_Problem problem;
		// Comparator<Solution> comparator = new
		// SingleObjectiveComparatorMinimum();
		// GKA algorithm;

		GKA_Problem problem;
		Comparator<Solution> comparator = new SingleObjectiveComparatorMinimum();
		GKA algorithm;

		System.out.println("Base: " + base + " (" + baseDados.numClasses()
				+ " grupos)");

		SolutionSet populationForPrint;

		System.out.println("FILE 1 - POR EXECUÇÃO");
		System.out
				.println("========================================================");
		System.out.println("#GROUPS\tEXECUTION\tDB\tRAND\tSILHOUETTE");

		Printer P = new Printer();

		for (int j = minGrupos; j <= maxGrupos; j++) {
			populationForPrint = new SolutionSet(100);

			for (int i = 1; i <= 10; i++) {
				// problem = new SimpleClusterNumberProblem(baseDados,
				// new JoaoCarlosDistance(), 0.8, j);
				// algorithm = new GKAMOD(problem);

				problem = new GKA_Problem(baseDados, j);
				algorithm = new GKA(problem);

				settingsLoader = new GKASettingsLoader(baseDados,
						new JoaoCarlosDistance(), mut_prob, populacao,
						geracoes, j);

				settingsLoader.loadSettingsFor(algorithm);
				SolutionSet population = algorithm.executar();

				System.out.print(j + "\t" + i + "\t");
				P.printIndexes(population.get(0), baseDados);
				System.out.println();

				populationForPrint.add(population.get(0));
				// Instances I = MainGKA.loadBase(population.get(0),
				// new Instances(baseDados));
				// String s = "GKAMOD\t" + i + "\t" + j + "\t" + I.numClasses()
				// + "\t" + BasicDaviesBouldin.main(I) + "\t"
				// + AdjustedRandIndex.ARI(baseDados, I) + "\n";

				// System.out.print(s);

			}

			System.out
					.println("========================================================");
			System.out.println("\nFILE 2 - SUMMARY");
			System.out
					.println("========================================================");

			P.printData(populationForPrint, comparator, baseDados);
			System.out
					.println("========================================================\n\n");

			P.cleanAll();

		}

	}

}
