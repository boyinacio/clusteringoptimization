package br.ufrn.application;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.cro.CROTWCV;
import jmetal.metaheuristics.cro.MaxCRO;
import jmetal.problems.clustering.GKA_Problem;
import jmetal.problems.clustering.SimpleClusterNumberProblem;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.Printer;
import zelig.util.distances.Distance;
import zelig.util.distances.JoaoCarlosDistance;
import br.ufrn.util.CROSettingsLoader;

public class MainCRO {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		LinkedList<String> bases = new LinkedList<String>();

		bases.add("autos");
		bases.add("sonar");
		bases.add("glass");
		bases.add("breast-cancer");
		bases.add("proteina_mod");
		bases.add("solar-flare");
		bases.add("ecoli_mod");
		bases.add("segment_mod");
		bases.add("ionosphere");
		bases.add("dermatology");

		for (String base : bases) {
			run(base);
		}
	}

	public static void run(String base) {
		Distance distance = new JoaoCarlosDistance();

		Instances baseDados = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		int size = baseDados.numClasses();
		int minGrupos, maxGrupos;

		if (size < 4) {
			minGrupos = 2;
			maxGrupos = 5;
		} else {
			minGrupos = size - 2;
			maxGrupos = size + 2;
		}

		GKA_Problem problem;
		// SimpleClusterNumberProblem problem;

		// CRO algorithm;
		// Comparator comparator = new SingleObjectiveComparatorMinimum();

		// MaxCRO algorithm;
		// Comparator<Solution> comparator = new
		// SingleObjectiveComparatorMaximum();

		CROTWCV algorithm;
		Comparator<Solution> comparator = new SingleObjectiveComparatorMinimum();

		// CROHS algorithm;
		// Comparator comparator = new SingleObjectiveComparatorMinimum();

		// MaxCROHS algorithm;
		// Comparator comparator = new SingleObjectiveComparatorMaximum();

		CROSettingsLoader settings;

		SolutionSet population;
		Solution best;

		int reefSizeM = 10;
		int reefSizeN = 10;
		int generations = 500;
		int opportunitiesToSettle = 3;

		double broadcastProb = 0.9;
		double predationProb = 0.1;
		double fractionCoralsDepredated = 0.1;
		double freeOccupiedProportion = 0.6;
		double EPS = 0.8;
		double probability = 0.05;

		settings = new CROSettingsLoader(reefSizeM, reefSizeN, generations,
				opportunitiesToSettle, broadcastProb, predationProb,
				fractionCoralsDepredated, freeOccupiedProportion, EPS,
				probability, baseDados, distance);

		System.out.println("Base: " + base + " (" + baseDados.numClasses()
				+ " grupos)");

		SolutionSet populationForPrint;
		Printer P = new Printer();

		for (int i = minGrupos; i <= maxGrupos; i++) {
			populationForPrint = new SolutionSet(100);

			System.out.println("FILE 1 - POR EXECUÇÃO");
			System.out
					.println("========================================================");
			System.out.println("#GROUPS\tEXECUTION\tDB\tRAND\tSILLHOUETTE");

			for (int exp = 1; exp <= 10; exp++) {
//				problem = new SimpleClusterNumberProblem(baseDados, distance,
//						EPS, i);
				 problem = new GKA_Problem(baseDados, i);

				 algorithm = new CROTWCV(problem);
//				algorithm = new MaxCRO(problem);
				// algorithm = new MaxCROHS(problem);

				settings.loadSettingsFor(algorithm);

				try {
					population = algorithm.execute();
					best = population.best(comparator);
					populationForPrint.add(best);

					System.out.print(i + "\t" + exp + "\t");
					P.printIndexes(best, baseDados);
					System.out.println();

				} catch (JMException e) {
					e.printStackTrace();
					System.exit(1);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					System.exit(1);
				}

			}

			System.out
					.println("========================================================");
			System.out.println("\nFILE 2 - SUMMARY");
			System.out
					.println("========================================================");

			P.printData(populationForPrint, comparator, baseDados);
			System.out
					.println("========================================================\n\n");

			P.cleanAll();

		}
	}

}
