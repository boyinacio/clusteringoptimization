package br.ufrn.application; // mudar o nome desse pacote para o do projeto do netbeans

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Formatter;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.SinglePointCrossover;
import jmetal.operators.mutation.BitFlipMutation;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.clustering.EnsembleProblem;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;
import weka.core.Instances;
import zelig.util.translators.ClusterNumberTranslator; // mudar esse import para o pacote do netbeans que você usa

public class MainEnsemble {

	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {

		FileReader reader = new FileReader("bases/all/iris.arff"); // Caminho da
																	// iris
																	// (aqui
																	// você
																	// troca
																	// para o
																	// diretório
																	// onde está
																	// o iris ou
																	// qualquer
																	// outra
																	// base que
																	// utilizes)
		Instances instancias = new Instances(reader);

		EnsembleProblem problem = new EnsembleProblem(instancias, 4); // Esse
																		// "4" é
																		// o
																		// número
																		// de
																		// grupos.
																		// Você
																		// também
																		// pode
																		// mudar
																		// a
																		// quantidade
																		// de
																		// grupos

		Algorithm algorithm; // The algorithm to use
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		HashMap parameters; // Operator parameters

		algorithm = new NSGAII(problem);

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", 100); // Tamanho da
															// população
		algorithm.setInputParameter("maxEvaluations", 20000); // Número de
																// gerações

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", 0.9);
		parameters.put("distributionIndex", 20.0);
		crossover = new SinglePointCrossover(parameters);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / problem.getNumberOfVariables());
		parameters.put("distributionIndex", 20.0);
		mutation = new BitFlipMutation(parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2",
				parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Add the indicator object to the algorithms
		// algorithm.setInputParameter("indicators", indicators);

		// Execute the Algorithm
		SolutionSet population = algorithm.execute();
		Solution melhor = population
				.best(new SingleObjectiveComparatorMinimum());

		Instances particao = new ClusterNumberTranslator().translate(melhor,
				instancias);

		// Salva em arquivo
		Formatter saver = null;

		try {
			saver = new Formatter("resultado_iris.txt"); // Especificar o nome
															// do arquivo para
															// salvar
		} catch (SecurityException securityException) {
			securityException.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		for (int i = 0; i < particao.numInstances(); i++) {
			saver.format("Instância %d - Grupo %d\n", i, ((Double) particao
					.instance(i).classValue()).intValue());
		}

		saver.close();

	} // main
} // NSGAII_main
