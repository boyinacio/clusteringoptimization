package br.ufrn.util;

import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.operators.crossover.GKAKMeansOperator;
import jmetal.operators.mutation.DistanceBasedMutation;
import jmetal.operators.selection.GKAMaxRouletteWheelSelection;
import jmetal.operators.selection.GKARouletteWheelSelection;
import weka.core.Instances;
import zelig.util.distances.Distance;

public class GKASettingsLoader implements AlgorithmSettingsLoader {

	private Distance distance;
	private double probability;
	private Instances baseDados;
	private int population, generations, numGrupos;

	private DistanceBasedMutation mutation;
	private GKAKMeansOperator kmeans;
	private GKARouletteWheelSelection selection;

	public GKASettingsLoader(Instances base, Distance distance,
			double probability, int population, int generations, int numGrupos) {
		this.baseDados = base;
		this.distance = distance;
		this.probability = probability;
		this.population = population;
		this.generations = generations;
		this.numGrupos = numGrupos;
	}

	@Override
	public void loadSettingsFor(Algorithm algorithm) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("base", this.baseDados);
		parameters.put("distance", this.distance);
		parameters.put("probability", this.probability);

		kmeans = new GKAKMeansOperator(parameters);
		mutation = new DistanceBasedMutation(parameters);
		selection = new GKARouletteWheelSelection(parameters);

		algorithm.setInputParameter("baseDados", this.baseDados);
		algorithm.setInputParameter("populationSize", this.population);
		algorithm.setInputParameter("maxEvaluations", this.generations);
		algorithm.setInputParameter("numGrupos", this.numGrupos);
		
		algorithm.addOperator("crossover", kmeans);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

	}

}
