package br.ufrn.util;

import jmetal.core.Algorithm;

public interface AlgorithmSettingsLoader {
	
	public void loadSettingsFor(Algorithm algorithm);

}
