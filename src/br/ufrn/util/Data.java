package br.ufrn.util;

public class Data implements Comparable<Data> {
	
	private int clusters;
	private double db;
	private double rand;
	
	public Data(){	
	}
	
	public Data(int clusters, double db, double rand) {
		this.clusters = clusters;
		this.db = db;
		this.rand = rand;
	}
	
	public double getDb() {
		return db;
	}
	public double getRand() {
		return rand;
	}
	
	public void setDb(double db) {
		this.db = db;
	}
	public void setRand(double rand) {
		this.rand = rand;
	}
	
	public int getClusters() {
		return clusters;
	}
	public void setClusters(int clusters) {
		this.clusters = clusters;
	}

	@Override
	public int compareTo(Data o) {
		double diff = this.rand - o.rand;		
		return (diff > 0)?1:((diff == 0)?0:-1);
	}
}
