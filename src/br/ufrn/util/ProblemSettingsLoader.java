package br.ufrn.util;

public interface ProblemSettingsLoader {
	
	public Object loadSettingsFor();

}
