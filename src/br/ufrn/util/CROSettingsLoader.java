package br.ufrn.util;

import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.operators.crossover.KMeansOperator;
import jmetal.operators.mutation.DistanceBasedMutation;
import jmetal.operators.selection.MaxRouletteWheelSelection;
import jmetal.operators.selection.RouletteWheelSelection;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;
import weka.core.Instances;
import zelig.util.distances.Distance;

public class CROSettingsLoader implements AlgorithmSettingsLoader {

	private KMeansOperator crossover; // Crossover operator
	private DistanceBasedMutation mutation; // Mutation operator
	private RouletteWheelSelection selection; // Selection operator

	private int reefSizeM, reefSizeN, generations;
	private int opportunitiesToSettle;

	private double broadcastProb, predationProb;
	private double fractionCoralsDepredated, freeOccupiedProportion;
	private double EPS;
	private double probability;

	private Instances base;
	private Distance distance;

	public CROSettingsLoader(int reefSizeM, int reefSizeN, int generations,
			int opportunitiesToSettle, double broadcastProb,
			double predationProb, double fractionCoralsDepredated,
			double freeOccupiedProportion, double ePS, double probability,
			Instances base, Distance distance) {
		super();
		this.reefSizeM = reefSizeM;
		this.reefSizeN = reefSizeN;
		this.generations = generations;
		this.opportunitiesToSettle = opportunitiesToSettle;
		this.broadcastProb = broadcastProb;
		this.predationProb = predationProb;
		this.fractionCoralsDepredated = fractionCoralsDepredated;
		this.freeOccupiedProportion = freeOccupiedProportion;
		EPS = ePS;
		this.probability = probability;
		this.base = base;
		this.distance = distance;
	}

	@Override
	public void loadSettingsFor(Algorithm algorithm) {
		algorithm.setInputParameter("generations", this.generations);

		algorithm.setInputParameter("reefSizeM", this.reefSizeM);
		algorithm.setInputParameter("reefSizeN", this.reefSizeN);

		algorithm.setInputParameter("broadcastProb", this.broadcastProb);
		algorithm.setInputParameter("predationProb", this.predationProb);

		algorithm.setInputParameter("fractionCoralsDepredated",
				this.fractionCoralsDepredated);
		algorithm.setInputParameter("freeOccupiedProportion",
				this.freeOccupiedProportion);

		algorithm.setInputParameter("opportunitiesToSettle",
				this.opportunitiesToSettle);

		HashMap<String, Object> parameters;

		parameters = new HashMap<String, Object>();
		parameters.put("base", this.base);
		parameters.put("distance", this.distance);
		parameters.put("EPS", this.EPS);
		parameters.put("HMRC", 0.3);
		parameters.put("comparator", new SingleObjectiveComparatorMinimum());
		// parameters.put("translator", new ClusterNumberTranslator());
		// parameters.put("problem", problem);
		// crossover = new HMCRprivate Crossover(parameters);
		crossover = new KMeansOperator(parameters);
		// crossover = new GKAKMeansOperator(parameters);
		// crossover = new DensityCrossover(parameters);
		// crossover = new StringCrossover(parameters);
		// crossover = new UniformCrossover(parameters);
		// crossover = new HeritageCrossover(parameters);

		parameters = new HashMap<String, Object>();
		parameters.put("base", this.base);
		parameters.put("distance", this.distance);
		parameters.put("probability", this.probability);
		parameters.put("PAR", 0.4);
		// mutation = new PARMutation(parameters);
		mutation = new DistanceBasedMutation(parameters);

		// parameters.put("initialProbability", 0.4);
		// parameters.put("finalProbability", 0.01);
		// parameters.put("generations", 500);
		// mutation = new GAKCMutation(parameters);

		parameters = new HashMap<String, Object>();
//		selection = new MaxRouletteWhellSelection(parameters);
		 selection = new RouletteWheelSelection(parameters);
		// selection = new GKARouletteWhellSelection(parameters);

		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

	}

}
