package br.ufrn.util;

import jmetal.problems.clustering.GKA_Problem;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class GKAProblemSettingsLoader implements ProblemSettingsLoader {

	private Instances base;
	private int numGrupos;
	
	public GKAProblemSettingsLoader(Instances base, int numGrupos) {
		super();
		this.numGrupos = numGrupos;
		this.base = base;
	}
		
	@Override
	public Object loadSettingsFor() {
		GKA_Problem problem = new GKA_Problem(base, numGrupos);
		return problem;
	}

}
